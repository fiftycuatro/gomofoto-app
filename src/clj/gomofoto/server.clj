(ns gomofoto.server
    (:require [com.stuartsierra.component :as component]
              [ring.adapter.jetty :refer [run-jetty]]
              [clojure.tools.logging :as log]
              [gomofoto.routes :as routes]))

(defrecord Server [port server]
    component/Lifecycle
    (start
      [{:keys [port] :as this}]
      (log/info "Starting on port " port)
      (let [server (run-jetty (routes/app) {:port port :join? false})]
        (assoc this :server server)))
    (stop
      [{:keys [server] :as this}]
      (log/info "Stopping")
      (.stop server)
      this))

(defn server
  [options]
  (map->Server options))
