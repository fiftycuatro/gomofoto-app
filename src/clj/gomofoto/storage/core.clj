(ns gomofoto.storage.core
    (:refer-clojure :exclude [get])
    (:require
        [com.stuartsierra.component :as component]
        [clojure.tools.logging :as log]
        [gomofoto.storage.filesystem]
        [gomofoto.storage.protocol :as proto]))

(defn get
    [storage bucket key]
    (when-not bucket
        (throw (ex-info "Expected bucket" {:type ::bucket-error})))
    (proto/get (:conn storage) bucket key))

(defn put
    [storage bucket key value]
    (when-not bucket
        (throw (ex-info "Expected bucket" {:type ::bucket-error})))
    (proto/put (:conn storage) bucket key value))

(defn delete
    [storage bucket key]
    (when-not bucket
        (throw (ex-info "Expected bucket" {:type ::bucket-error})))
    (proto/delete (:conn storage) bucket key))

(defrecord Storage
    [conn]
    component/Lifecycle
    (start [this]
        (log/info "Starting storage with " this)
        (assoc this :conn (proto/connect this)))
    (stop [this]
        (proto/close (:conn this))))

(defn storage
    [options]
    (map->Storage options))
