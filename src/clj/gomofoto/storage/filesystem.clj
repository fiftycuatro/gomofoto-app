(ns gomofoto.storage.filesystem
    (:require
        [clojure.java.io :as io]
        [clojure.string :as string]
        [clojure.tools.logging :as log]
        [gomofoto.storage.protocol :as proto])
    (:import java.io.File))


(defn construct-path [base bucket key]
    (string/join (File/separator) [base bucket key]))

(defn ensure-bucket [base bucket]
    (let [bucket-dir (->> [base bucket]
                          (string/join (File/separator))
                          clojure.java.io/as-file)]
        (log/info "Ensure bucket exists " base " " bucket)
        (when-not (.exists bucket-dir)
            (log/info "Creating bucket: " bucket)
            (.mkdir bucket-dir))))

(defrecord FilesystemStorage [base-path save-agent]
    proto/IStorage
    (get [this bucket key]
        (let [path (construct-path base-path bucket key)]
        (when (.exists (clojure.java.io/as-file path))
            (read-string (slurp (construct-path base-path bucket key))))))
    (put [this bucket key value]
        (send-off save-agent
            (fn [_]
                (log/info "Saving into bucket")
                (let [file-path (construct-path base-path bucket key)
                      tmp-path (str file-path ".tmp")]

                    (ensure-bucket base-path bucket)
                    (spit tmp-path (prn-str value))
                    (.renameTo (File. tmp-path) (File. file-path))))))
    (delete [this bucket key]
        (send-off save-agent
            (fn [_]
                (let [file-path (construct-path base-path bucket key)]
                    (io/delete-file file-path)))))
    (close [this]))

(defmethod proto/connect :filesystem
    [{:keys [storage-path] :as options}]
    (.mkdir (File. storage-path))
    (->FilesystemStorage storage-path (agent nil :error-mode :continue)))
