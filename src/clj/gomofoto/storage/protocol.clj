(ns gomofoto.storage.protocol
    (:refer-clojure :exclude [get]))

(defprotocol IStorage
    (get [this bucket key])
    (put [this bucket key value])
    (delete [this bucket key])
    (close [this]))

(defmulti connect :storage)
