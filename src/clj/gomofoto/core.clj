(ns gomofoto.core
  (:require
    [clojure.tools.logging :as log]
    [clojure.string :as string]
    [clojure.tools.cli :refer [parse-opts]]
    [com.stuartsierra.component :as component]
    [gomofoto.system :as system])
  (:gen-class))

(def required-opts #{})

(defn missing-required? [opts]
    (not-every? opts required-opts))

(def cli-options
    [["-p" "--port PORT" "Server Port"
      :default 8082
      :parse-fn #(Integer/parseInt %)
      :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]
     [nil "--storage-type" "Storage Type"
      :id :storage
      :default :filesystem
      :parse-fn #(keyword %)
      :validate[#(contains? #{:filesystem} %)]]
     [nil "--storage-path" "Storage Path"
      :default "./storage-buckets"]
     ["-h" "--help"]])

(defn usage [options-summary]
    (->> ["Bundox Documentation Server"
          ""
          "Usage: bundox [options]"
          ""
          "Options:"
          options-summary]
          (string/join \newline)))

(defn required-error-msg [opts]
    (str "The following options are required:\n\n"
         (string/join \newline (clojure.set/difference required-opts opts))))

(defn error-msg [errors]
    (str "The following errors occured while parsing your command:\n\n"
         (string/join \newline errors)))

(defn exit [status msg]
    (println msg)
    (System/exit status))

(defn -main [& args]
  (let [{:keys [options errors summary]} (parse-opts args cli-options)]
    (println options)
    (cond
      (:help options) (exit 0 (usage summary))
      (missing-required? options) (exit 0 (required-error-msg options))
      errors (exit 1 (error-msg errors)))
    :default (do
               (component/start (system/app-system options)))))
