(ns gomofoto.routes
  (:require
    [clojure.tools.logging :as log]
    [compojure.handler]
    [compojure.core :as compojure :refer [GET POST DELETE]]
    [compojure.route :refer [resources files]]
    [ring.util.response :refer [response content-type resource-response]]
    [ring.middleware.transit :refer [wrap-transit-response wrap-transit-body]]))

(defn empty-response [f]
  (try (f)
       {:status 200 :body {:message "success"}}
       (catch Exception e
         {:status 500 :body {:message (.getMessage e)}})))

(defn api-routes
  []
  (compojure/routes
    (GET "/version" [] (response {:product "gomofoto" :version "1.0.0"}))))

(defn static-routes
  []
  (compojure/routes
    (GET "/" [] (content-type
                  (resource-response "index.html" {:root "public"}) "text/html"))
    (resources "/")))

(defn gomofoto-api
  []
  (-> (api-routes)
      compojure.handler/api
      (wrap-transit-response {:encoding :json :opts {}})
      (wrap-transit-body {:keywords? true :opts {}})))

(defn app []
  (compojure/routes
    (gomofoto-api)
    (static-routes)))

