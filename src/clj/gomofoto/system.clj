(ns gomofoto.system
  (:require
    [com.stuartsierra.component :as component]
    [gomofoto.storage.core :as storage]
    [gomofoto.server :as server]))

(def system-components [:server :storage])

(defrecord AppSystem [config]
  component/Lifecycle
  (start [this]
    (component/start-system this system-components))
  (stop [this]
    (component/stop-system this system-components)))

(defn app-system
  [options]
  (map->AppSystem
    {:server (server/server options)
     :storage (storage/storage options)}))
