(ns gomofoto.db
  (:require [cljs.reader]
            [cljs.spec :as s]))

;; -- Spec --------------------------------------------------------------------

(s/def ::role string?)
(s/def ::db (s/keys :req-un [::role]))

;; -- Default app-db Value  ---------------------------------------------------
;;

(def default-value
  {:role  "guest"})

