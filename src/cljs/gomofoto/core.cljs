(ns gomofoto.core
  (:require-macros [secretary.core :refer [defroute]])
  (:require
            [goog.events :as events]
            [reagent.core :as reagent]
            [re-frame.core :refer [dispatch dispatch-sync clear-subscription-cache!]]
            [secretary.core :as secretary]
            [gomofoto.events]
            [gomofoto.subs]
            [gomofoto.views]
            [devtools.core :as devtools])
  (:import [goog History]
           [goog.history EventType]))


;; -- Debugging aids ----------------------------------------------------------
(devtools/install!)       ;; we love https://github.com/binaryage/cljs-devtools
(enable-console-print!)   ;; so println writes to console.log

;; -- Routes and History ------------------------------------------------------

(defroute "/" [] (dispatch [:startup]))

(def history
  (doto (History.)
        (events/listen EventType.NAVIGATE
                       (fn [event] (secretary/dispatch! (.-token event))))
        (.setEnabled true)))

;; -- Lifecycle ---------------------------------------------------------------

(defn start-polling []
  (js/setTimeout
    (fn []
      (start-polling)) 5000))

;; -- Entry Point -------------------------------------------------------------

(defn mount-root []
  (reagent/render [gomofoto.views/gomofoto-app] (.getElementById js/document "app")))

(defn ^:export main
  []
  (dispatch-sync [:initialise-db])
  (mount-root))
