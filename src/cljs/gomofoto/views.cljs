(ns gomofoto.views
  (:require [reagent.core  :as reagent]
            [re-frame.core :refer [subscribe dispatch]]
            [cljsjs.material-ui]))

;;==============================================================================
;; Material-UI Setup
;;==============================================================================

;; Macroized version of adapting the material-ui components. Doesn't quite work yet
(def material-tags
  ['RaisedButton 'Paper 'Divider 'TextField])

(defn material-ui-react-import
  [tname]
  '(def ~tname
     (reagent.core/adapt-react-class (aget js/MaterialUI ~(name tname)))))

(defmacro export-material-ui-react-classes
  []
  '(do
     ~@(map material-ui-react-import material-tags)))

;; Once I learn how to write a macro this can be replaced with a list of components to adapt
(def Paper (-> js/MaterialUI (aget "Paper") (reagent.core/adapt-react-class)))
(def Divider (-> js/MaterialUI (aget "Divider") (reagent.core/adapt-react-class)))
(def RaisedButton (-> js/MaterialUI (aget "RaisedButton") (reagent.core/adapt-react-class)))
(def TextField (-> js/MaterialUI (aget "TextField") (reagent.core/adapt-react-class)))
(def MuiThemeProvider (-> js/MaterialUIStyles (aget "MuiThemeProvider") (reagent.core/adapt-react-class)))

;;==============================================================================
;; Panel Definitions
;;==============================================================================

(defn sample-panel
  []
  (let [style {:marginLeft 20}]
    (fn []
      [MuiThemeProvider
       [Paper {:zDepth 2
               :style {:width "400px" :padding 20 :margin 20}}
        [:h1 "Profile"]
        [TextField {:style style
                    :floatingLabelText "First Name"}]
        [TextField {:style style
                    :floatingLabelText "Last Name"}]
        [Divider]
        [RaisedButton {:style (assoc style :marginTop 20)
                       :label "Save" :primary true}]
        ]]
      )))

(defn splash
  []
  [:div.splash-container
   [:div.splash
    [:h1.splash-head
     [:span [:img {:src "/img/logo_round_white_border.png"}]]
     "gomofoto"]
    [:p.splash-subhead "Summer 2017"]]])

(defn gomofoto-app
  []
  [splash])

