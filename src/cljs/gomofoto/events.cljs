(ns gomofoto.events
  (:require
    [gomofoto.db   :refer [default-value]]
    [re-frame.core :refer [reg-event-db reg-event-fx inject-cofx path trim-v
                           after debug]]
    [cljs.spec     :as s]))

;; -- Interceptors --------------------------------------------------------------
;;
(defn check-and-throw
      "throw an exception if db doesn't match the spec."
      [a-spec db]
      (when-not (s/valid? a-spec db)
                (throw (ex-info (str "spec check failed: " (s/explain-str a-spec db)) {}))))

(def check-spec-interceptor (after (partial check-and-throw :gomofoto.db/db)))

;; the chain of interceptors we use for all handlers that manipulate nodes
(def node-interceptors [check-spec-interceptor              ;; ensure the spec is still valid
                        (path :nodes)                        ;; 1st param to handler will be the value from this path
                        (when ^boolean js/goog.DEBUG debug)  ;; look in your browser console for debug logs
                        trim-v])                             ;; removes first (event id) element from the event vec


;; -- Event Handlers ----------------------------------------------------------

(reg-event-fx
  :initialise-db
  [check-spec-interceptor]
  (fn [{:keys [db]} _]
      {:db default-value}))

(reg-event-fx
  :startup
  [check-spec-interceptor]
  (fn [{:keys [db]} _]
    {:db db}))
