(ns user
    (:require [com.stuartsierra.component :as component]
              [clojure.tools.namespace.repl :refer [refresh]]
              [figwheel-sidecar.system :as sys]
              [gomofoto.system :refer [app-system]]))

(def system nil)

(defn init []
  (alter-var-root #'system
                  (constantly
                    (component/system-map
                      :figwheel-system (sys/figwheel-system (sys/fetch-config))
                      :css-watcher (sys/css-watcher {:watch-paths ["resources/public/css"]})
                      :app-system (app-system {:port 8082
                                               :storage :filesystem
                                               :storage-path "./storage-buckets"})))))

(defn start []
  (alter-var-root #'system component/start))

(defn stop []
  (alter-var-root #'system
                  (fn [s] (when s (component/stop s)))))

(defn go []
  (init)
  (start)
  nil)

(defn reset []
  (stop)
  (refresh :after 'user/go))
