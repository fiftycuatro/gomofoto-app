FROM openjdk:8

ADD target/gomofoto*-standalone.jar /gomofoto-standalone.jar

EXPOSE 8082

CMD ["java", "-jar", "/gomofoto-standalone.jar"]
