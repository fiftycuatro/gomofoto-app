(defproject gomofoto "1.0.0-SNAPSHOT"
  :description "Photo sharing app for families"
  :url "http://gomofoto.com"
  :license {:name "The MIT License (MIT)" }
  :dependencies [
                 ;; Clojure
                 [org.clojure/clojure "1.8.0"]
                 [org.clojure/tools.logging "0.3.1"]
                 [com.stuartsierra/component "0.3.1"]
                 [compojure/compojure "1.5.1"]
                 [ring/ring-core "1.5.0"]
                 [ring/ring-jetty-adapter "1.5.0"]
                 [org.clojure/tools.cli "0.3.5"]
                 [com.cognitect/transit-clj "0.8.295"]
                 [ring-transit "0.1.6"]
                 [org.clojure/core.async "0.2.395"]

                 ;; Clojurescript
                 [org.clojure/clojurescript "1.9.229"]
                 [reagent "0.6.0" :exclusions [cljsjs/react cljsjs/react-dom]]
                 [re-frame "0.9.2"]
                 [binaryage/devtools "0.8.1"]
                 [secretary "1.2.3"]
                 [day8.re-frame/http-fx "0.1.3"]
                 [com.cognitect/transit-cljs "0.8.237"]
                 [cljs-ajax "0.5.8"]
                 [cljsjs/material-ui "0.19.0-0"]

                 ;; Utils
                 [org.jcodec/jcodec "0.1.9"]
                 [ch.qos.logback/logback-classic "1.1.7"]
                 [org.slf4j/jcl-over-slf4j "1.7.21"]
                 [org.slf4j/jul-to-slf4j "1.7.21"]
                 [org.slf4j/log4j-over-slf4j "1.7.21"]
                 [com.google.guava/guava "20.0"]]

  :source-paths ["src/clj"]

  :clean-targets ^{:protect false} ["resources/public/js/compiled" "target"]

  :plugins [[lein-cljsbuild "1.1.3"]
            [lein-ancient "0.6.10"]]

  :profiles {:uberjar {:aot        :all
                       :prep-tasks ["compile" ["cljsbuild" "once"]]
                       :main       gomofoto.core}
             :dev {:plugins [[lein-figwheel "0.5.9"]]
                   :source-paths ["dev/clj" "src/clj" "src/cljs"]
                   :dependencies [[org.clojure/tools.namespace "0.2.10"]
                                  [org.clojure/java.classpath "0.2.3"]
                                  [figwheel-sidecar "0.5.4-6"]]}}
  :cljsbuild
  {:builds
   [{:id "min"
     :source-paths ["src/cljs"]
     :compiler     {:main gomofoto.core
                    :output-to  "resources/public/js/compiled/app.js"
                    :optimizations :advanced
                    :closure-defines {goog.DEBUG false}
                    :pretty-print false}}]})
